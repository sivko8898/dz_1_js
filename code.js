/* 1. 
    Змінну можно оголосити за допомогою
    директив: var - засторіла;
    let, const;*/

/* 2. 
    prompt від confirm відризняется тим що:
    prompt показуе сповіщення та запрошуе введєння відповіді 
    від користувача в полі введення, та при натисканні ОК (повертае відповідь),
    як що cancel чи ESC (повертае null).

    А confirm показуе сповіщення, та очикує відповіді від
    користувача у вигляді натискання ОК (повертае true),
    та cancel чи ESC (повертае false).*/

/* 3.
     Не явне перетворення типів це: 
    коли значення можуть бути конвертовані між різними
    типами автоматично
    
    1 == null
    
    null + new Date().*/

// №1
    let Name = ("Олександр");
    let admin = ("Олександр");
    console.log(Name, admin);

// №2

    //Вибачте за самодияльность.

    let days = (7);
    const hours = 24, minuts = 60, seconds = 60;
    console.log(`days ${hours * minuts * seconds * days}s`);

    //також є другий вариант

    /* let days = prompt("Will select a day from 1 to 10");
    const hours = 24, minuts = 60, seconds = 60;
    alert("Thank you");
    console.log(`days ${hours * minuts * seconds * days}s`); */
    
// №3
    let userCity = prompt("Your hometown");
    alert(`This is your hometown ${userCity}`);
    console.log (userCity);